package org.alytsee.springboot.mocking.mocktesting.spike;

import static org.junit.Assert.*;

import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

public class JsonAssertTest {
	
	String actualResponse1 = "{\"id\":1,\"name\":\"ball\",\"price\":10,\"quantity\":100}";
	String actualResponse2 = "{\"id\":1 ,\"name\":\"ball\",\"price\": 10}";

	@Test
	public void jsonAssert_test() throws JSONException {
		String expectedResponse1 = "{id:1,name:ball,price:10,quantity:100}";

		String expectedResponse2 = "{\"id\":1,\"name\":\"ball\",\"price\":10,\"quantity\":100}";
		JSONAssert.assertEquals(expectedResponse1, actualResponse1, true);
		JSONAssert.assertEquals(expectedResponse2, actualResponse1, true);

	//	JSONAssert.assertEquals(expectedResponse, actualResponse2, false); // when true there is strict check ,it has to match structure(entire string)
	}

}
