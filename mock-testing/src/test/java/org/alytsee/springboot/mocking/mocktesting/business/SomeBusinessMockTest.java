package org.alytsee.springboot.mocking.mocktesting.business;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.alytsee.springboot.mocking.mocktesting.data.SomeDataService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class SomeBusinessMockTest {

	
	@InjectMocks
	SomeBusinessImpl business = new SomeBusinessImpl();
	@Mock
  	SomeDataService dataServiceMock;
	
	//SomeDataService dataServiceMock = mock(SomeDataService.class);
	// if we do inject mock and mock, the setter(i.e on before method) will get automatically called , just add RunWith annotation
	
	/*@Before // execute before every test
	public void before() {
		business.setSomeDataService(dataServiceMock);	
	}*/
	
	
	@Test
	public void calculateSumUsingDataService_stub_test() {
		
		when(dataServiceMock.retrieveAllData()).thenReturn(new int[] {1,2,3});
		// business.setSomeDataService(dataServiceMock);
		//int actualResult = business.calculateSumUsingDataService();
		//int expectedResult = 6;
		assertEquals(6, business.calculateSumUsingDataService());
	}
     
	@Test
	public void calculateSum_empty_test() {
		
		when(dataServiceMock.retrieveAllData()).thenReturn(new int[] {});
		//business.setSomeDataService(dataServiceMock);
		assertEquals(0, business.calculateSumUsingDataService());
	}
	
	@Test
	public void calculateSum_oneValue_test() {
		
		when(dataServiceMock.retrieveAllData()).thenReturn(new int[] {5});
		//business.setSomeDataService(dataServiceMock);
		//int actualResult = business.calculateSumUsingDataService();
		//int expectedResult = 5;
		assertEquals(5, business.calculateSumUsingDataService());
	}
}
