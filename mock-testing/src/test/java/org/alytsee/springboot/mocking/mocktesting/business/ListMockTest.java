package org.alytsee.springboot.mocking.mocktesting.business;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

public class ListMockTest {
	
List<String> mock = Mockito.mock(List.class);

	@Test
	public void size_basic_test() {
		List mock = Mockito.mock(List.class);
		when(mock.size()).thenReturn(5);
		assertEquals(5, mock.size());
	}
	
	@Test
	public void returnDifferentValue_test() {
		List mock = Mockito.mock(List.class);
		when(mock.size()).thenReturn(5).thenReturn(10);
		assertEquals(5, mock.size());
		assertEquals(10, mock.size());
	}
	
	@Test
	public void returnWithParameters_test() {
		List mock = Mockito.mock(List.class);
		when(mock.get(0)).thenReturn("kasi viswanath");
		assertEquals("kasi viswanath", mock.get(0));
	}
    
	@Test
	public void returnWithGenericParameters_test() {
		List mock = Mockito.mock(List.class);
		when(mock.get(anyInt())).thenReturn("kasi viswanath");
		assertEquals("kasi viswanath", mock.get(0));
		assertEquals("kasi viswanath", mock.get(1));
	}
	
	@Test
	public void verification_basic_test() {
		String value = mock.get(0);
		
		// verify , get method is called on mock
		verify(mock).get(0);
		verify(mock).get(anyInt());
		verify(mock, atLeast(1)).get(0);
		verify(mock, never()).get(1);

	}
	
	
	@Test
	public void argumentCapture_test() {
		// someDataservice.storeSum(sum)  capturing sum and testing
		mock.add("SomeString");
		
		// verification
		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
		verify(mock).add(captor.capture()); // verifying add method call and capture the string
		assertEquals("SomeString",captor.getValue());
	}
	
	@Test
	public void multipleArgumentCapture_test() {
		// someDataservice.storeSum(sum)  capturing sum and testing
		mock.add("SomeString1");
		mock.add("SomeString2");
		// verification
		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);  
		verify(mock, times(2)).add(captor.capture()); // verifying add method call and capture the string
		List<String> allValues =  captor.getAllValues();  // get all values return list
		assertEquals("SomeString1",allValues.get(0));
		assertEquals("SomeString2",allValues.get(1));
	}
	
	@Test
	public void mocking() {
		ArrayList arrayListMock = Mockito.mock(ArrayList.class);
		System.out.println(arrayListMock.get(0));  // null
		System.out.println(arrayListMock.size());  //  0
		arrayListMock.add("test1");
		arrayListMock.add("test2");
		System.out.println(arrayListMock.size()); // 0
		when(arrayListMock.size()).thenReturn(5);
		System.out.println(arrayListMock.size()); // 5
	}
	
	@Test
	public void spying() {   // spy retains the original behaviour
		ArrayList arrayListSpy = spy(ArrayList.class);
		// arrayListSpy.add("test1");
		// System.out.println(arrayListSpy.get(0));  // exception (indexoutofbound exception )
		
		System.out.println(arrayListSpy.size());  //  0
		arrayListSpy.add("test1");
		arrayListSpy.add("test2");
		System.out.println(arrayListSpy.size()); // 2
		when(arrayListSpy.size()).thenReturn(5);
		System.out.println(arrayListSpy.size()); // 5
		arrayListSpy.add("test3");
		System.out.println(arrayListSpy.size()); // 5
		
	}	
}
