package org.alytsee.springboot.mocking.mocktesting.data;

import static org.junit.Assert.*;

import java.util.List;

import org.alytsee.springboot.mocking.mocktesting.model.Item;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ItemRepositoryTest {
    
	@Autowired
	ItemRepository repository;
	
	@Test
	public void findAll_test() {
		List<Item> items = repository.findAll();
        assertEquals(3, items.size());
	}

}
