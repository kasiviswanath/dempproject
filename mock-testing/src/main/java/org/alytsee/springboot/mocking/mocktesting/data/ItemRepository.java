package org.alytsee.springboot.mocking.mocktesting.data;

import org.alytsee.springboot.mocking.mocktesting.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Integer> {
	
}
