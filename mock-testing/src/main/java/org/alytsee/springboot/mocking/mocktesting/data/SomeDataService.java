package org.alytsee.springboot.mocking.mocktesting.data;

public interface SomeDataService {

	int[] retrieveAllData();

}
