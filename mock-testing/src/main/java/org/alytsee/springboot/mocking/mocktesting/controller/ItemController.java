package org.alytsee.springboot.mocking.mocktesting.controller;

import java.util.List;

import org.alytsee.springboot.mocking.mocktesting.business.ItemBusinessService;
import org.alytsee.springboot.mocking.mocktesting.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemController {

@Autowired
ItemBusinessService itemBusinessService;
	
	@GetMapping("/dummy-item")
	public Item  dummyItem() {
	 return new Item(1,"ball",10,100); 
	}
   
   @GetMapping("/item-from-business-service")
  	public Item  itemFromBusinessService() { 
  	 return itemBusinessService.retrieveHardCodedItem();
  	}
   
   @GetMapping("/all-items-from-database")
   public List<Item> retrieveAllItems(){
	   return itemBusinessService.retrieveAllItems();
   }
}
