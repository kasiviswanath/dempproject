package org.alytsee.springboot.mocking.mocktesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockTestingApplication {
	public static void main(String[] args) {
		SpringApplication.run(MockTestingApplication.class, args);
	}
}

