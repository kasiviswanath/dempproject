package org.alytsee.springboot.mocking.mocktesting.business;

import java.util.List;

import org.alytsee.springboot.mocking.mocktesting.data.ItemRepository;
import org.alytsee.springboot.mocking.mocktesting.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemBusinessService {
	
	@Autowired
	private ItemRepository repository;
	
	
	public List<Item> retrieveAllItems(){
		//return repository.findAll();
		List<Item> items = repository.findAll();
		for(Item item: items) {
			item.setValue(item.getPrice() * item.getQuantity());
		}
		return items;
	}

	public Item retrieveHardCodedItem() {
		return new Item(1,"ball",10,100);
	}

}
